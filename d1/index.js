console.log("Hello, world!")

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operators

// Basic Assignment Operator
let assignmentNumber = 8;

// Addition Assignment Operator

assignmentNumber = assignmentNumber + 2;
console.log("Result of additoin operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction, multiplication, division assignment operators (-=, *=, /=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple operators and parentheses
/*
    - When multiple operators are applied in a single statement, it follows
    PEMDAS (Parenthesis, exponents, multiplication, division, addition and
    subtraction) rule.

    1. 3 * 4 = 12
    2. 12 / 5 = 2.4
    3. 1 + 2
    4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

/*
    - By adding our parentheses to create more complex computations will change
    the order of operations still following the same rule
    1. 4/5 = 0.8
    2. 2-3 = -1
    3. -1 * 0.8 = -0.8
    4. 1 + -0.8 = 0.2
*/

// Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to.

let z = 1;
let increment = ++z;
console.log("Result of pre increment operator: " + increment); // 2
console.log("Result of pre increment operator: " + z); // 2

increment = z++;
console.log("Result of post increment operator: " + increment); // 2
console.log("Result of post increment operator: " + z); // 3

let decrement = --z;
console.log("Result of pre decrement operator: " + decrement); // 2
console.log("Result of pre decrement operator: " + z); // 2

decrement = z--;
console.log("Result of post decrement operator: " + decrement); // 2
console.log("Result of post decrement operator: " + z); // 2

// Type coercion

let numA = '10';
let numB = 12;

/*
    - adding/concatenating a string and a number will result in a string
*/

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

/*
  The result is a number
*/

let nonCoercion = numC + numD;
console.log(nonCoercion); // 30
console.log(typeof nonCoercion); // number

/*
    booleans are cast to num: false = 0; true = 1;
*/

let numE = true + 1;
console.log(numE); // 2

let numF = false + 1;
console.log(numF); // 1

// Comparison operators

let juan = 'juan';

 // Equality Operator (==)
/*
    - checks whether the operands are equal/have the same content
    - converts and compares operands of different types
*/
console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == '1'); // true
console.log(0 == false); // true
console.log('juan' == 'juan'); // true
console.log('juan' == juan); // true

// Inequality operator

/*
  - checks whether the operands are note equal or have different content
  - converts first
*/


console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != '1'); // false
console.log(0 != false); // false
console.log('juan' != 'juan'); // false
console.log('juan' != juan); // false

// Strict Equality Operator (===)
/*
  Same type and content
*/

console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1 === '1'); // false
console.log(0 === false); // false
console.log('juan' === 'juan'); // true
console.log('juan' === juan); // true

// Strict Inequality Operator (===)
/*
  Same type and content
*/

console.log(1 !== 1); // false
console.log(1 !== 2); // true
console.log(1 !== '1'); // true
console.log(0 !== false); // true
console.log('juan' !== 'juan'); // false
console.log('juan' !== juan); // false

// Relationship status

let a = 50;
let b = 65;

let isGreaterThan = a > b;
let isLessThan = a < b;
let isGTorEqual = a >= b;
let isLTorEqual = a <= b;

console.log(isGreaterThan); // false
console.log(isLessThan); // true
console.log(isGTorEqual); // false
console.log(isLTorEqual); // true

let numStr = "30";
console.log(a > numStr); // casts string to number
console.log(b <= numStr); // false

let str = "twenty";
console.log(b >= str);
// false
// because string is not numeric
// String is converted to NaN

// Logical operators (&&, ||, !)

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);

// "typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is
